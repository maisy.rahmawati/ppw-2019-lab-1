from django.urls import path
from . import views

app_name = 'mywebsite'

urlpatterns = [
    path('', views.front2, name='front2'),
    path('galery/', views.front3, name='front3'),
]
