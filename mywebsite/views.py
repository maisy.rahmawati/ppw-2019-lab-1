from django.shortcuts import render

# Create your views here.
def front1(request):
    return render(request, 'front1.html')

def front2(request):
    return render(request, 'front2.html')

def front3(request):
    return render(request, 'front3.html')
